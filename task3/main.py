import io

import searcher
import sys
from pathlib import Path
import re
import spacy
import nltk.tokenize
from invertize import invertize, get_searcher_func, get_file_names

INPUT_IO = sys.stdin
OUTPUT_IO = sys.stdout
TOKENIZER = nltk.tokenize.word_tokenize

TOKENS_FILE: Path = Path("tokens.txt")
LEMMAS_FILE: Path = Path("lemmas.txt")
FILTERED_DIR: Path = Path("../filtered/")
INDEX: Path = Path("index.txt")
INVERTED_INDEX: Path = Path("inverted_index.txt")
# NLP пакет для русского языка
LANGUAGE: spacy.Language = spacy.load("ru_core_news_lg", disable=["parser", "senter", "attribute_ruler", "ner"])
# Выбираем только слова из русских букв+дефис между частями слов
TOKENS_SELECTORS: [str] = ["^[А-яЁё][А-яЁё-]*[А-яЁё]$"]
# Выбираем только глаголы, существительные, прилагательные
TAGS_SELECTORS: [str] = ["VERB", "PROPN", "NOUN", "ADJ"]

# Для парсера
OPENING_BRACKET = re.compile(r"\(")
CLOSING_BRACKET = re.compile(r"\)")
AND = re.compile(r"([Aa][Nn][Dd])|&")
OR = re.compile(r"([Oo][Rr])|\|")
NOT = re.compile(r"([Nn][Oo][Tt])|~")
LEMMA = re.compile(r"^[А-яЁё][А-яЁё-]*[А-яЁё]$")
IGNORED = re.compile(r" ")

if __name__ == "__main__":
    if not FILTERED_DIR.is_dir() or not INDEX.is_file() or not TOKENS_FILE.is_file() or not LEMMAS_FILE.is_file():
        raise ValueError("Bad constants")
    item = next(FILTERED_DIR.iterdir(), None)
    if item == None or item.name == ".placeholder":
        raise ValueError("Empty filtered archive")

    files: set[str]

    if not INVERTED_INDEX.is_file() or INVERTED_INDEX.stat().st_size == 0:
        print("inverted index was not found, producing...")
        files = invertize(index_file=INDEX, inverted_index_file=INVERTED_INDEX, filtered_folder=FILTERED_DIR,
                  tokens_selectors=[re.compile(regexp) for regexp in TOKENS_SELECTORS],
                  tags_selectors=[re.compile(regexp) for regexp in TAGS_SELECTORS],
                  nlp=LANGUAGE, lemmas_file=LEMMAS_FILE)
        print("inverted index was produced")
    else:
        print("inverted index was found")
        files = set(get_file_names(index_file=INDEX))

    search = get_searcher_func(INVERTED_INDEX)

    parser = searcher.get_parser(
        {OPENING_BRACKET: 0, CLOSING_BRACKET: 1, AND: 2, OR: 3, NOT: 4, LEMMA: 5, IGNORED: 255})
    searcher.listen(input_io=INPUT_IO, output_io=OUTPUT_IO, tokenizer=TOKENIZER, parser=parser, lemmatizer=LANGUAGE, searcher=search, files=files)
