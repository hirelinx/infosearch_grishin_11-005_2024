from pathlib import Path
import re
import spacy
import typing
import ast

def get_file_names(index_file: Path) -> [str]:
    filenames: [str] = []
    with open(index_file, "r") as index:
        # Читаем строку
        for line in index:
            # Делим строку по ": "
            strips = line.split(": ")
            # Добавляем файл
            filenames.append(strips[0])
    return filenames
def invertize(index_file: Path, inverted_index_file: Path, filtered_folder: Path, lemmas_file: Path,
              tokens_selectors: [re.Pattern], tags_selectors: [re.Pattern],
              nlp: spacy.Language) -> set[str]:
    # Проходимся по каждому шаблону, если один из них совпадает, возвращаем True
    # noinspection DuplicatedCode
    def is_selected(string: str, regexps: [re.Pattern]):
        for regexp in regexps:
            if re.match(regexp, string):
                return True
        return False

    # Словарь лемм и файлов в виде {"лемма":[""]}
    lemmas_pool = {}
    # Список лемм, которые встречаются во всех файлах (на текущий момент)
    lemmas_always_pool = {}
    # Вытаскиваем имена файлов из индекса

    filenames: [str] = get_file_names(index_file)

    with open(lemmas_file, "r") as lems_file:
        for lem in lems_file:
            # Делим строку по ": "
            lemma = lem[:lem.find(":")]
            # Добавляем файл
            lemmas_pool[lemma] = []

    def get_add_to_pool():
        past_file_str_i = 0

        # Маска, встретилась ли лемма из списка выше

        def _add_to_pool(ind, file_str_i: int):
            lemmas_pool[lemmas_always_pool[ind]] = [filenames[indd] for indd in range(file_str_i)]
            return False

        # Называется: с генераторами с детства я дружу. Функция добавления в результирующий бассейн
        def add_to_pool(token, file_str_i):
            nonlocal past_file_str_i, lemmas_always_pool
            if file_str_i == 0:
                if token.lemma_ not in lemmas_always_pool:
                    lemmas_always_pool[token.lemma_] = True
                return True
            if file_str_i != past_file_str_i:
                temp = {}
                for key, value in lemmas_always_pool.items():
                    if value == False:
                        lemmas_pool[key] = [filenames[inn] for inn in range(file_str_i)]
                    else:
                        temp[key] = False
                    lemmas_always_pool = temp
                past_file_str_i = file_str_i
            for strin in lemmas_always_pool:
                if strin == token.lemma_:
                    lemmas_always_pool[strin] = True
                    return True
            if token.lemma_ in lemmas_pool:
                if filenames[file_str_i] not in lemmas_pool[token.lemma_]:
                    lemmas_pool[token.lemma_].append(filenames[file_str_i])
                    return True
                else:
                    return True

            raise ValueError("""configuration inconsistency: lemmas were not extracted as intended by the selectors, 
                or the index was changed after the lemmas had been extracted.""")

        return add_to_pool

    for i, file_string in enumerate(filenames):
        # Считываем отфильтрованную выкачку
        with open(filtered_folder.joinpath(file_string), "r") as file:
            doc = file.read()
            # Анализируем отфильтрованное
            tokens = nlp(doc)

            add_to_pool = get_add_to_pool()
            # Добавляем в результат, если токен не из стоп-листа, подходит по одному из шаблонов тэг, подходит по
            # одному из шаблонов токен
            # noinspection PyStatementEffect
            [None for token in tokens if
             not token.is_stop and is_selected(token.pos_, tags_selectors) and is_selected(token.text, tokens_selectors)
             and add_to_pool(token, i)]
            # Для прогресса
            print(f"{file_string} is parsed")

    # Записываем результат
    with open(inverted_index_file, "w+") as inverted_index_file:
        for key, value in sorted(lemmas_pool.items()):
            if len(value) == 0:
                if key in lemmas_always_pool:
                    # noinspection PyUnboundLocalVariable
                    output = {"count": i + 1, "inverted_array": [], "word": key}
                else:
                    raise ValueError("""configuration inconsistency: lemmas were not extracted as intended by the 
                    selectors, or the index was changed after the lemmas had been extracted.""")
            else:
                output = {"count": len(value), "inverted_array": value, "word": key}
            inverted_index_file.write("".join([repr(output), "\n"]))

    return set(filenames)

def get_searcher_func(inverted_index_file: Path) -> typing.Callable[[str],tuple[int, set[str], str]|None]:
    index: typing.Dict[str, tuple[int, list[str]]] = {}
    files: set[str] = set()
    with open(inverted_index_file, "r") as index_file:
        for line in index_file:
            ind: typing.Dict[str, int | list[str] | str] = ast.literal_eval(line[:-1])
            files.union(ind["inverted_array"])
            index[ind["word"]] = (ind["count"], ind["inverted_array"])


    def search(lemma: str) -> None | tuple[int, set[str], str]:
        nonlocal index
        if lemma not in index:
            return (0, set(), lemma)
        else:
            subres = index[lemma]
            return (subres[0], set(subres[1]), lemma)

    re_func = search

    return re_func
