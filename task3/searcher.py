import re
import typing

import spacy
import sympy
from sympy.logic.boolalg import to_dnf

OPENING_BRACKET = 0
CLOSING_BRACKET = 1
AND = 2
OR = 3
NOT = 4
LEMMA = 5
IGNORED = 255
LEMMAS_VARS_START = 256
LEMMAS_VARS_STOP = 511
UNCLASSIFIED = -1


def get_parser(classifier: typing.Mapping[re.Pattern, int]):
    # for value in classifier.values():
    #     if LEMMAS_VARS_START <= value <= LEMMAS_VARS_STOP:
    #         raise ValueError("bad classifier")

    def parse(tokens: [str]) -> (list[int], bool):
        result = []
        has_unclassified = False
        breaks = False
        mask = UNCLASSIFIED
        for token in tokens:
            for key in classifier:
                if key.match(token):
                    mask = classifier[key]
                    breaks = True
                    break
            if not breaks:
                has_unclassified = True
            result.append(mask)
            mask = UNCLASSIFIED
            breaks = False
        return result, has_unclassified

    return parse


def _try_to_simplify(tokens_n_tags: list[tuple[str, int]]):
    var_names: [str] = [chr(name_i) for name_i in range(ord('a'), ord('z'))]

    # lemma: var
    variables_vars: [sympy.Symbol] = []
    variables_tokens: [str] = []
    form: [str] = []
    for token, tag in tokens_n_tags:
        if OPENING_BRACKET == tag:
            form.append(' ( ')
        elif LEMMA == tag:
            breaks = False
            for i, lemma in enumerate(variables_tokens):
                breaks = False
                if lemma == token:
                    var = variables_vars[i]
                    breaks = True
                    break
            if not breaks:
                var = var_names.pop(0)
                variables_tokens.append(token)
                variables_vars.append(sympy.Symbol(var))
            # noinspection PyUnboundLocalVariable
            form.append(''.join([" ", str(var), " "]))
        elif CLOSING_BRACKET == tag:
            form.append(" ) ")
        elif AND == tag:
            form.append(" & ")
        elif OR == tag:
            form.append(" | ")
        elif NOT == tag:
            form.append(" ~ ")



    try:
        res = sympy.sympify(''.join([*form, "| z & ~z"]))
        expr = to_dnf(res, simplify=True, force=True)
    except sympy.SympifyError or TypeError:
        return None
    return expr, list([(str(variable), tok) for variable, tok in zip(variables_vars, variables_tokens) if
                       variable in expr.binary_symbols])


def _to_query_form(line: str):
    return ''.join([char for char in line if ord(char) not in [ord(charac) for charac in [" ", "(", ")", "&"]]])


def _query_search(query_form: str, vars_n_tokens: typing.Dict[str, str],
                  searcher: typing.Callable[[str], tuple[int, set[str], str] | None], files: set[str]) -> (
        dict[str, int | set[str] | str] | None):
    files_len = len(files)
    previous_not: bool = False
    word: str = ""
    result: tuple[int, set[str]] | None
    result = None
    and_result: tuple[tuple[int, set[str]] | None, tuple[int, set[str]] | None] | None
    and_result = None
    sequence : int = 0

    def get_real_search():
        subindex = {}

        for token_ in vars_n_tokens.values():
            searched = searcher(token_)
            subindex[token_] = searched

        def _real_search(lemma: str):
            nonlocal subindex
            return subindex[lemma]

        return _real_search

    real_search = get_real_search()

    def append_to_result():
        nonlocal and_result, result, files, files_len

        effective_and_set: set[str]

        if and_result is None:
            return


        if result is None:
            pos_and = and_result[0]
            neg_and = and_result[1]
            if pos_and is None and neg_and is None:
                return
            if pos_and is not None:
                length, pos_set = pos_and
                if length < files_len:
                    result = length, pos_set
                else:
                    result = files_len, files
                return
            if pos_and is None:
                length, neg_set = neg_and
                if length < files_len:
                    effective_and_set = files.difference(neg_set)
                    result = (files_len - length, effective_and_set)
                else:
                    effective_and_set = set()
                    result = (0, effective_and_set)
                return
            raise ValueError("logic violation")

        res_len, res_set = result
        pos_and = and_result[0]
        neg_and = and_result[1]


        if pos_and is None and neg_and is None:
            raise ValueError("logic violation")
        elif pos_and is not None:
            pos_length, pos_set = pos_and
            if pos_length < files_len:
                union = pos_set.union(res_set)
                result = len(union), union
            else:
                result = files_len, files
            return
        elif pos_and is None:
            neg_length, neg_set = neg_and
            if neg_length < files_len:
                effective_and_set = files.difference(neg_set)
                union = effective_and_set.union(res_set)
                result = (len(union), union)
            else:
                effective_and_set = files
                result = (0, effective_and_set)
            return
        else:
            raise ValueError("logic violation")

    def append_to_and(subresult: tuple[tuple[int, set[str]] | None, tuple[int, set[str]] | None] | None):
        nonlocal and_result, result, files, files_len, sequence
        new_negative: tuple[int, set[str]]
        new_positive: tuple[int, set[str]]


        def preconflux() -> (tuple[int, set[str]] | None, tuple[int, set[str]] | None):
            nonlocal and_result, result, files, files_len
            if subresult is None:
                new_positive_ = and_result[0]
                new_negative_ = and_result[1]
                return new_positive_, new_negative_
            if and_result is None:
                new_positive_ = subresult[0]
                new_negative_ = subresult[1]
                return new_positive_, new_negative_

            def unpack(x):
                if x is not None:
                    return x[0], x[1]
                else:
                    return (None, None)

            pos_len, pos_set = unpack(subresult[0])
            neg_len, neg_set = unpack(subresult[1])

            pos_and_len, pos_and_set = unpack(and_result[0])
            neg_and_len, neg_and_set = unpack(and_result[1])

            _new_negative = conflux(sub_len=neg_len, sub_set=neg_set, and_len=neg_and_len, and_set=neg_and_set,
                                   func=set.union)
            _new_positive = conflux(sub_len=pos_len, sub_set=pos_set, and_len=pos_and_len, and_set=pos_and_set,
                                   func=set.intersection)

            return _new_positive, _new_negative

        def conflux(sub_len, and_len: int, sub_set, and_set: set[str], func: typing.Callable[[set, set], set]) -> tuple[
                                                                                                                      int,
                                                                                                                      set[
                                                                                                                          str]] | None:
            nonlocal and_result, result, files, files_len
            if sub_len is not None and sub_len == files_len or and_len is not None and and_len == files_len:
                new = (files_len, set())
            else:
                if sub_set is not None and and_set is not None:
                    new_set = func(and_set, sub_set)
                    new_length = len(new_set)
                elif sub_set is None and and_set is not None:
                    new_set = and_set
                    new_length = len(new_set)
                elif sub_set is not None and and_set is None:
                    new_set = sub_set
                    new_length = len(new_set)
                else:
                    return None
                if new_length == files_len:
                    new_set = set()
                new = (new_length, new_set)
            return new

        def eliminate(positive: tuple[int, set[str]] | None, negative: tuple[int, set[str]] | None) -> tuple[int, set[str]] | None:
            nonlocal and_result, result, files, files_len
            if negative is None:
                return positive
            if positive is None:
                return positive
            if negative[0] == files_len:
                return 0, set()
            effective_positive_set: set[str]
            if positive[0] == files_len:
                if negative[0] == 0:
                    return positive
                effective_positive_set = files
            else:
                effective_positive_set = positive[1]

            new_positive_set = effective_positive_set.difference(negative[1])
            new_positive_length = len(new_positive_set)

            if new_positive_length == files_len:
                new_positive_set = set()

            return new_positive_length, new_positive_set

        new_positive, new_negative = preconflux()
        eliminated_positive = eliminate(positive=new_positive, negative=new_negative)

        and_result = (eliminated_positive, new_negative)

    for i in range(len(query_form)):
        char = query_form[i]
        match char:
            case "~":
                previous_not = True
            case "|":
                sequence = 0
                word = ''.join([word, "OR "])
                previous_not = False
                append_to_result()
                and_result = None
                pass
            case _:
                breaks = False
                for key in vars_n_tokens.keys():
                    if key == char:
                        token = vars_n_tokens[char]
                        breaks = True
                        break
                if not breaks:
                    raise ValueError("Malformed query")

                if sequence == 0:
                    # noinspection PyUnboundLocalVariable
                    if previous_not:
                        word = ''.join([word,"NOT ", token, " "])
                    else:
                        word = ''.join([word, token, " "])
                else:
                    if previous_not:
                        word = ''.join([word, "AND NOT ", token, " "])
                    else:
                        word = ''.join([word,"AND ", token," "])

                sequence = sequence+1

                # noinspection PyUnboundLocalVariable
                sub_result = real_search(token)
                res: tuple[int, set[str]] | None
                if sub_result is None:
                    res = None
                else:
                    res = (sub_result[0], sub_result[1])
                if previous_not:
                    append_to_and((None, res))
                else:
                    append_to_and((res, None))
                previous_not = False

    append_to_result()

    if result is not None:
        count, filez = result
        return {"count": count, "inverted_array": filez, "word": word[:-1]}

    else:
        return None


def listen(input_io: typing.TextIO, output_io: typing.TextIO, tokenizer, parser, lemmatizer: spacy.Language,
           searcher: typing.Callable[[str], tuple[int, set[str], str] | None], files: set[str]):
    while True:
        output_io.write("waiting for input...\n")
        line = input_io.readline()
        if line == "":
            break
        tokens = tokenizer(line[:-1])
        out = [token for token in tokens]
        parsed, has_unclassified = parser(out)
        tokens_and_tags = list(zip(out, parsed))
        output_io.write(f"{tokens_and_tags}\n")
        output_io.write(f"has unclassified: {has_unclassified}\n")
        tokens_and_tags = [(token, tag) if tag != 5 else (lemmatizer(token)[0].lemma_, tag) for token, tag in
                           tokens_and_tags]
        output_io.write(f"lemmatized: {tokens_and_tags}\n")
        if not has_unclassified:
            ret = _try_to_simplify(tokens_and_tags)
            if ret is None:
                output_io.write(f"malformed query\n")
                continue
            simplified, vars_n_tokens = ret
            if str(simplified) == "False":
                output_io.write("search rejected: it always False query\n")
                continue
            if str(simplified) == "True":
                output_io.write("search rejected: it always True query\n")
                continue
            output_io.write("search was started\n")
            output_io.write(f"simplified: {simplified}\n")
            output_io.write(f"var to token map: {vars_n_tokens}\n")
            query_form = _to_query_form(str(simplified))
            output_io.write(f"query_form: {query_form}\n")
            if simplified == False:
                output_io.write(f"nothing was found: query always false\n")
            result = _query_search(query_form, dict(vars_n_tokens), searcher, files)
            if result is not None:
                lst = list(result["inverted_array"])
                lst.sort()
                fancy_out = {"count": result["count"], "inverted_array":lst, "word": result["word"]}
                output_io.write(f"found: {fancy_out}\n")
            else:
                output_io.write(f"nothing was found: {result}\n")
        else:
            output_io.write("search rejected\n")
    output_io.write("exit\n")
