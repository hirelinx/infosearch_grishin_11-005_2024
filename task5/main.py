import re
import sys
from pathlib import Path

import nltk
import spacy

import searcher
from invertize import get_file_names, get_lemma_idf_provider
from tokens import get_lemmas_n_tokens
from vectorize import get_searcher

INPUT_IO = sys.stdin
OUTPUT_IO = sys.stdout
TOKENIZER = nltk.tokenize.word_tokenize

TOKENS_FILE: Path = Path("tokens.txt")
LEMMAS_FILE: Path = Path("lemmas.txt")
FILTERED_DIR: Path = Path("../filtered/")
INDEX: Path = Path("index.txt")
INVERTED_INDEX: Path = Path("inverted_index.txt")
TOKENS_TF_IDF_FOLDER: Path = Path("../tf-idfs/tokens")
LEMMAS_TF_IDF_FOLDER: Path = Path("../tf-idfs/lemmas")

# NLP пакет для русского языка
LANGUAGE: spacy.Language = spacy.load("ru_core_news_lg", disable=["parser", "senter", "attribute_ruler", "ner"])
# Выбираем только слова из русских букв+дефис между частями слов
TOKENS_SELECTORS: [str] = ["^[А-яЁё][А-яЁё-]*[А-яЁё]$"]
# Выбираем только глаголы, существительные, прилагательные
TAGS_SELECTORS: [str] = ["VERB", "PROPN", "NOUN", "ADJ"]

# Для парсера
LEMMA = re.compile(r"^[А-яЁё][А-яЁё-]*[А-яЁё]$")
IGNORED = re.compile(r" ")

if __name__ == "__main__":
    if (not FILTERED_DIR.is_dir() or not INDEX.is_file() or not TOKENS_FILE.is_file() or not LEMMAS_FILE.is_file() or
            not TOKENS_TF_IDF_FOLDER.is_dir() or not LEMMAS_TF_IDF_FOLDER.is_dir() or not INVERTED_INDEX.is_file()):
        raise ValueError("Bad constants")

    files = get_file_names(index_file=INDEX)

    idf_provider = get_lemma_idf_provider(inverted_index_file=INVERTED_INDEX, length=len(files))

    lemmas_n_tokens = get_lemmas_n_tokens(lemmas_file=LEMMAS_FILE)

    search = get_searcher(tf_idf_folder=LEMMAS_TF_IDF_FOLDER, files=files,
                          all_pretenders={lemma for lemma in lemmas_n_tokens}, idf_provider=idf_provider, )

    parser = searcher.get_parser(
        {LEMMA: 5, IGNORED: 255})
    searcher.listen(input_io=INPUT_IO, output_io=OUTPUT_IO, tokenizer=TOKENIZER, parser=parser, lemmatizer=LANGUAGE,
                    searcher=search, tags_selectors=TAGS_SELECTORS, tokens_selectors=TOKENS_SELECTORS)

    print("\nexit main\n")
