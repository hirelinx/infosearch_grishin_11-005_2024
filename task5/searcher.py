import re
import typing

import spacy

LEMMA = 5
IGNORED = 255
UNCLASSIFIED = -1


def get_parser(classifier: typing.Mapping[re.Pattern, int]) -> typing.Callable[[list[str]], tuple[list[int], bool]]:
    def parse(tokens: [str]) -> (list[int], bool):
        result = []
        has_unclassified = False
        breaks = False
        mask = UNCLASSIFIED
        for token in tokens:
            for key in classifier:
                if key.match(token):
                    mask = classifier[key]
                    breaks = True
                    break
            if not breaks:
                has_unclassified = True
            result.append(mask)
            mask = UNCLASSIFIED
            breaks = False
        return result, has_unclassified

    return parse


def listen(input_io: typing.TextIO, output_io: typing.TextIO, tokenizer, parser, lemmatizer: spacy.Language,
           tokens_selectors: [re.Pattern], tags_selectors: [re.Pattern],
           searcher: typing.Callable[[list[str]], list[tuple[str, float]]]):
    while True:
        output_io.write("waiting for input...\n")
        line = input_io.readline()
        if line == "":
            break
        tokens = tokenizer(line[:-1])
        out = [token for token in tokens]
        parsed, has_unclassified = parser(out)
        tokens_and_tags = list(zip(out, parsed))
        output_io.write(f"tokens and tags{tokens_and_tags}\n")
        output_io.write(f"has unclassified: {has_unclassified}\n")

        filtered_tokens = [token for token, tag in
                           tokens_and_tags if tag == LEMMA]
        output_io.write(f"filtered: {filtered_tokens}\n")

        def is_selected(string: str, regexps: [re.Pattern]):
            for regexp in regexps:
                if re.match(regexp, string):
                    return True
            return False

        selected_tokens = [token.lemma_ for token in lemmatizer(" ".join(tokens))
                           if is_selected(token.text, tokens_selectors) and is_selected(token.pos_, tags_selectors)]

        output_io.write(f"selected tokens: {selected_tokens}\n")

        if len(selected_tokens) != 0:
            output_io.write("search was started\n")
            result: list[tuple[str, float]] = searcher(selected_tokens)
            output_io.write(f"found: {result}\n")
        else:
            output_io.write("search was rejected\n")

    output_io.write("exit listen\n")
