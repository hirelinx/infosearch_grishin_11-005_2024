import bisect
import math
import typing
from pathlib import Path


def _get_vector_of_file(copied_vector: dict[str, float], file: Path, coef_selector_i: int):
    with open(file, "r") as file_f:
        for line in file_f:
            strips = line.split(" ")
            copied_vector[strips[0]] = float(strips[coef_selector_i])


def _get_cosin(search_vector: dict[str, float], file_vector: dict[str, float]) -> float:
    search_for_root: float = 0.0
    file_for_root: float = 0.0

    cosin: float = 0.0

    for key in search_vector:
        search_coef = search_vector[key]
        search_for_root = search_for_root + search_coef ** 2

        file_coef = file_vector[key]
        file_for_root = file_for_root + file_coef ** 2

        cosin = cosin + search_coef * file_coef

    sqrt = math.sqrt(search_for_root * file_for_root)
    if sqrt == 0.0:
        return 0.0
    else:
        return cosin / sqrt


def _get_vector_of_search(tokens: list[str], copied_vector: dict[str, float],
                          idf_provider: typing.Callable[[str], float], coef_selector_i: int):
    tokens.sort()

    freqs_sum = len(tokens)

    prev_token = tokens[0]
    # tf, tf-idf
    temp: list = [0.0, 0.0]

    skip = False

    prev_skipped = False

    prev_legal_token: str = ""

    for token in tokens:
        if skip:
            if prev_token == token:
                continue
            else:
                prev_skipped = True
                skip = False

        if token not in copied_vector:
            skip = True
            prev_token = token
            continue
        else:
            prev_legal_token = token

        if prev_token != token:
            if prev_skipped:
                prev_skipped = False
            else:
                # tf
                temp[0] = temp[0] / freqs_sum
                # tf-idf
                temp[1] = temp[0] * idf_provider(token)

                copied_vector[token] = temp[coef_selector_i]

                temp = [0.0, 0.0]
        # freqs
        temp[0] = temp[0] + 1.0
        prev_token = token

    if prev_legal_token != "":
        temp[0] = temp[0] / freqs_sum
        # tf-idf
        # noinspection PyUnboundLocalVariable
        temp[1] = temp[0] * idf_provider(prev_legal_token)

        copied_vector[prev_legal_token] = temp[coef_selector_i]


def get_searcher(tf_idf_folder: Path, files: list[str], all_pretenders: set[str],
                 idf_provider: typing.Callable[[str], float], top_N: int = 10, tf_or_idf: bool = False) \
        -> typing.Callable[[list[str]], list[tuple[str, float]]]:
    val_i = 0 if tf_or_idf else 1

    base_vector: dict[str, float] = {pretender: 0.0 for pretender in all_pretenders}

    def vector_range(tokens: list[str]) -> list[tuple[str, float]]:
        nonlocal tf_idf_folder, val_i, files, base_vector, idf_provider, top_N

        cosins: list[tuple[str, float]] = []

        search_vector: dict[str, float] = {key: value for key, value in base_vector.items()}

        _get_vector_of_search(tokens=tokens, copied_vector=search_vector, idf_provider=idf_provider,
                              coef_selector_i=val_i)

        for file_name in files:
            file_vector: dict[str, float] = {key: value for key, value in base_vector.items()}
            _get_vector_of_file(copied_vector=file_vector, file=tf_idf_folder.joinpath(file_name),
                                coef_selector_i=val_i)

            pretender = _get_cosin(search_vector=search_vector, file_vector=file_vector)

            if pretender == 0.0:
                continue

            def debug(item) -> float:
                key, value = item
                return value

            index = bisect.bisect_left(cosins, pretender, key=debug)

            if len(cosins) < top_N:
                cosins = [*cosins[:index], (file_name, pretender), *cosins[index:]]

        return sorted(cosins, key=lambda item: item[1], reverse=True)

    return vector_range
