import ast
import math
import typing
from pathlib import Path


def get_file_names(index_file: Path) -> [str]:
    filenames: [str] = []
    with open(index_file, "r") as index:
        # Читаем строку
        for line in index:
            # Делим строку по ": "
            strips = line.split(": ")
            # Добавляем файл
            filenames.append(strips[0])
    return filenames


def get_lemma_idf_provider(inverted_index_file: Path, length: int, log_base: float | int = 10) \
        -> typing.Callable[[str], float]:
    idf_of_lemmas: dict[str, float] = {}

    with open(inverted_index_file, "r") as index_file:
        for line in index_file:
            ind: typing.Dict[str, int | list[str] | str] = ast.literal_eval(line[:-1])
            idf_of_lemmas[ind["word"]] = math.log(length / ind["count"], log_base)

    def idf_provide(token: str) -> float:
        nonlocal idf_of_lemmas
        return idf_of_lemmas.get(token, 0.0)

    return idf_provide
