import typing


from internal import INDEX_FILE,  TOKENS_FILE, LEMMAS_FILE, TOKENS_TF_IDF_FOLDER, LEMMAS_TF_IDF_FOLDER, INVERTED_INDEX, LEMMA, IGNORED, TOKENIZER, LANGUAGE, TAGS_SELECTORS, TOKENS_SELECTORS
from internal.searcher  import get_parser, get_searcher
from internal.index import load_index
from internal.invertize import get_lemma_idf_provider
from internal.tokens import get_lemmas_n_tokens
from internal.vectorize import get_vectorizer

SEARCHER: typing.Callable[[str], tuple[list[tuple[str, float, str]] | None, int]]
INDEX: dict[str]

def main():
    print("main init\n")

    if (not INDEX_FILE.is_file() or not TOKENS_FILE.is_file() or not LEMMAS_FILE.is_file() or
            not TOKENS_TF_IDF_FOLDER.is_dir() or not LEMMAS_TF_IDF_FOLDER.is_dir() or not INVERTED_INDEX.is_file()):
        raise ValueError("Bad constants")

    index = load_index(index_file=INDEX_FILE)

    global INDEX
    INDEX = index

    idf_provider = get_lemma_idf_provider(inverted_index_file=INVERTED_INDEX, length=len(index))

    lemmas_n_tokens = get_lemmas_n_tokens(lemmas_file=LEMMAS_FILE)

    search = get_vectorizer(tf_idf_folder=LEMMAS_TF_IDF_FOLDER, files=list(index.keys()),
                            all_pretenders={lemma for lemma in lemmas_n_tokens}, idf_provider=idf_provider)

    parser = get_parser({LEMMA: 5, IGNORED: 255})

    global SEARCHER
    SEARCHER = get_searcher(tokenizer=TOKENIZER, parser=parser, lemmatizer=LANGUAGE,
                              searcher=search, tags_selectors=TAGS_SELECTORS, tokens_selectors=TOKENS_SELECTORS, index=index)

    print("main init is completed\n")

main()