from pathlib import Path


def load_index(index_file: Path):
    result: dict[str, str] = {}

    with open(index_file, "r") as index:
        for line in index:
            strips = line[:-1].split(" ")
            result[strips[0][:-1]] = strips[1]

    return result