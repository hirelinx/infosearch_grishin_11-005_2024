import pathlib


def get_lemmas_n_tokens(lemmas_file: pathlib.Path) -> dict[str, list[str]]:
    result: dict[str, list[str]] = {}

    with open(lemmas_file, "r") as lem_file:
        for line in lem_file:
            strips: list[str] = line.split(" ")
            strips[0] = strips[0][:-1]

            result[strips[0]] = strips[1:]

    return result
