import re
import typing
from pathlib import Path
import nltk
import spacy
import sys

INPUT_IO = sys.stdin
OUTPUT_IO = sys.stdout
TOKENIZER = nltk.tokenize.word_tokenize

TOKENS_FILE: Path = Path("internal/tokens.txt")
LEMMAS_FILE: Path = Path("internal/lemmas.txt")
FILTERED_DIR: Path = Path("../filtered")
INDEX_FILE: Path = Path("internal/index.txt")
INVERTED_INDEX: Path = Path("internal/inverted_index.txt")
TOKENS_TF_IDF_FOLDER: Path = Path("../tf-idfs/tokens")
LEMMAS_TF_IDF_FOLDER: Path = Path("../tf-idfs/lemmas")

# NLP пакет для русского языка
LANGUAGE: spacy.Language = spacy.load("ru_core_news_lg", disable=["parser", "senter", "attribute_ruler", "ner"])
# Выбираем только слова из русских букв+дефис между частями слов
TOKENS_SELECTORS: [str] = ["^[А-яЁё][А-яЁё-]*[А-яЁё]$"]
# Выбираем только глаголы, существительные, прилагательные
TAGS_SELECTORS: [str] = ["VERB", "PROPN", "NOUN", "ADJ"]

# Для парсера
LEMMA = re.compile(r"^[А-яЁё][А-яЁё-]*[А-яЁё]$")
IGNORED = re.compile(r" ")

from internal import main

SEARCHER: typing.Callable[[str], tuple[list[tuple[str, float, str]] | None, int]] = main.SEARCHER
INDEX: dict[str] = main.INDEX
