import logging
import re
import typing

import spacy

LEMMA = 5
IGNORED = 255
UNCLASSIFIED = -1


def get_parser(classifier: typing.Mapping[re.Pattern, int]) -> typing.Callable[[list[str]], tuple[list[int], bool]]:
    def parse(tokens: [str]) -> (list[int], bool):
        result = []
        has_unclassified = False
        breaks = False
        mask = UNCLASSIFIED
        for token in tokens:
            for key in classifier:
                if key.match(token):
                    mask = classifier[key]
                    breaks = True
                    break
            if not breaks:
                has_unclassified = True
            result.append(mask)
            mask = UNCLASSIFIED
            breaks = False
        return result, has_unclassified

    return parse


def get_searcher(tokenizer, parser, lemmatizer: spacy.Language,
                 tokens_selectors: [re.Pattern], tags_selectors: [re.Pattern],
                 searcher: typing.Callable[[list[str]], tuple[list[tuple[str, float]], int]], index:dict[str, str]) \
        -> typing.Callable[[str], tuple[list[tuple[str, float, str]] | None, int]]:
    def seacrh(line: str) -> tuple[list[tuple[str, float, str]] | None, int]:
        nonlocal tokenizer, parser, lemmatizer, tokens_selectors, tags_selectors, searcher

        logging.debug(f"search line provided: {line}")
        if line == "":
            logging.debug(f"search rejected")
            return None, -1
        tokens = tokenizer(line[:-1])
        out = [token for token in tokens]
        parsed, has_unclassified = parser(out)
        tokens_and_tags = list(zip(out, parsed))
        logging.debug(f"tokens and tags{tokens_and_tags}\n")
        logging.debug(f"has unclassified: {has_unclassified}\n")

        filtered_tokens = [token for token, tag in tokens_and_tags if tag == LEMMA]

        logging.debug(f"filtered: {filtered_tokens}\n")

        def is_selected(string: str, regexps: [re.Pattern]):
            for regexp in regexps:
                if re.match(regexp, string):
                    return True
            return False

        selected_tokens = [token.lemma_ for token in lemmatizer(" ".join(tokens))
                           if is_selected(token.text, tokens_selectors) and is_selected(token.pos_, tags_selectors)]

        logging.debug(f"selected tokens: {selected_tokens}\n")

        if len(selected_tokens) != 0:
            logging.debug("search was started\n")
            result: list[tuple[str, float]]
            found:int
            result, found = searcher(selected_tokens)

            result_with_url: list[tuple[str, float, str]] = [(file, score, index[file]) for file, score in result]
            logging.debug(f"found: {result_with_url}\n")
            logging.debug(f"found {found} results\n")
            return result_with_url, found
        else:
            logging.debug("search was rejected\n")
            return None, -1

    return seacrh

