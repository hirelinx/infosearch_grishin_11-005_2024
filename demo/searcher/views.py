from django.http import HttpResponse
from searcher import SEARCHER
from django.shortcuts import render


def index(request):
    return render(request, "searcher/index.html", {})


def search(request):
    text = request.GET['text'].replace("+", " ")
    searched: list[tuple[str, float, str]]
    found: int
    searched, found = SEARCHER(text)
    context = {"searched": searched, "found": found}
    return render(request, "searcher/search.html", context)
