import math
import re
from pathlib import Path
import spacy


def get_file_names(index_file: Path) -> [str]:
    filenames: [str] = []
    with open(index_file, "r") as index:
        # Читаем строку
        for line in index:
            # Делим строку по ": "
            strips = line.split(": ")
            # Добавляем файл
            filenames.append(strips[0])
    return filenames


def _count_tokens_freq_for_file(filtered_folder: Path, file_path: Path, tokens_selectors: [re.Pattern],
                                tags_selectors: [re.Pattern],
                                nlp: spacy.Language) -> tuple[dict[str, float], dict[str, float]]:
    # Проходимся по каждому шаблону, если один из них совпадает, возвращаем True
    def is_selected(string: str, regexps: [re.Pattern]):
        for regexp in regexps:
            if re.match(regexp, string):
                return True
        return False

    # {token1: freq1, token2: freq2}
    token_freqs: dict[str, float] = {}
    # {lemma1: freq1, lemma2: freq2}
    lemma_freqs: dict[str, float] = {}
    freqs_sum: int = 0

    # Называется: с генераторами с детства я дружу. Функция добавления в результирующий бассейн
    def add_to_pool(token):
        nonlocal freqs_sum
        freqs_sum = freqs_sum + 1
        if token.lemma_ in lemma_freqs:
            lemma_freqs[token.lemma_] = lemma_freqs[token.lemma_] + 1
        else:
            lemma_freqs[token.lemma_] = 1

        if token.text in token_freqs:
            token_freqs[token.text] = token_freqs[token.text] + 1
        else:
            token_freqs[token.text] = 1
        return True

        # Считываем отфильтрованную выкачку

    with open(filtered_folder.joinpath(file_path), "r") as file:
        doc = file.read()


    tokens = nlp(doc)
    # noinspection PyStatementEffect
    [None for token in tokens if not token.is_stop and is_selected(token.pos_, tags_selectors) and
     is_selected(token.text, tokens_selectors) and add_to_pool(token)]

    def get_result(what: dict[str, float]):
        nonlocal freqs_sum

        for key in what:
            what[key] = what[key] / freqs_sum

    get_result(lemma_freqs)
    get_result(token_freqs)

    return lemma_freqs, token_freqs


def tf_idf(tokens_tf_idf_folder: Path, lemmas_tf_idf_folder: Path, index_file: Path, filtered_folder: Path,
           tokens_selectors: [re.Pattern], tags_selectors: [re.Pattern],
           nlp: spacy.Language):
    # Вытаскиваем имена файлов из индекса
    filenames: [str] = get_file_names(index_file=index_file)

    # lemma:occurrences
    lemma_occurrence: dict[str, float] = {}
    # token:occurrences
    token_occurrence: dict[str, float] = {}
    documents: int = len(filenames)

    # {file_name1: ({lemma1: freq1, lemma2: freq2}, {token1: freq1, token2: freq2}),
    #  file_name2: ({lemma1: freq1, lemma2: freq2}, {token1: freq1, token2: freq2}), ...}
    documents_freqs: dict[str, tuple[dict[str, float], dict[str, float]]] = {}

    def add_to(to: dict[str, float], what: str):
        if what in to:
            to[what] = to[what] + 1
        else:
            to[what] = 1

    def calculate_idfs(what: dict[str, float]):
        for key in what:
            what[key] = math.log10(documents / what[key])

    for file_string in filenames:
        lemma_freqs, token_freqs = _count_tokens_freq_for_file(filtered_folder=filtered_folder,
                                                               file_path=Path(file_string),
                                                               tokens_selectors=tokens_selectors,
                                                               tags_selectors=tags_selectors, nlp=nlp)
        print(f"{file_string} is parsed")

        for key in lemma_freqs:
            add_to(lemma_occurrence, key)

        for key in token_freqs:
            add_to(token_occurrence, key)

        documents_freqs[file_string] = (lemma_freqs, token_freqs)

    print(f"all files were parsed")

    calculate_idfs(lemma_occurrence)
    calculate_idfs(token_occurrence)

    for file_name in filenames:
        lemma_tfs, token_tfs = documents_freqs[file_name]

        def write(dir_out, occurrences, tfs):
            nonlocal file_name
            with open(dir_out.joinpath(file_name), "w+") as file:
                for key, idf in sorted(occurrences.items()):
                    tf = tfs.get(key, None)

                    if tf is None:
                        file.write(f"{key} {0.0} {0.0}\n")
                    else:
                        idf = occurrences[key]
                        file.write(f"{key} {tf} {tf * idf}\n")

            print(f"{dir_out.joinpath(file_name).absolute().resolve()} was written")

        # Записываем результат

        write(dir_out=lemmas_tf_idf_folder, occurrences=lemma_occurrence, tfs=lemma_tfs)
        write(dir_out=tokens_tf_idf_folder, occurrences=token_occurrence, tfs=token_tfs)
