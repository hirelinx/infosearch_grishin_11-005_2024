import sys
from pathlib import Path
import re
import spacy
from coefficients import tf_idf

INPUT_IO = sys.stdin
OUTPUT_IO = sys.stdout

TOKENS_FILE: Path = Path("tokens.txt")
LEMMAS_FILE: Path = Path("lemmas.txt")
FILTERED_DIR: Path = Path("../filtered/")
INDEX: Path = Path("index.txt")
INVERTED_INDEX: Path = Path("inverted_index.txt")
TOKENS_TF_IDF_FOLDER: Path = Path("../tf-idfs/tokens")
LEMMAS_TF_IDF_FOLDER: Path = Path("../tf-idfs/lemmas")

# NLP пакет для русского языка
LANGUAGE: spacy.Language = spacy.load("ru_core_news_lg", disable=["parser", "senter", "attribute_ruler", "ner"])
# Выбираем только слова из русских букв+дефис между частями слов
TOKENS_SELECTORS: [str] = ["^[А-яЁё][А-яЁё-]*[А-яЁё]$"]
# Выбираем только глаголы, существительные, прилагательные
TAGS_SELECTORS: [str] = ["VERB", "PROPN", "NOUN", "ADJ"]

# Для парсера
OPENING_BRACKET = re.compile(r"\(")
CLOSING_BRACKET = re.compile(r"\)")
AND = re.compile(r"([Aa][Nn][Dd])|&")
OR = re.compile(r"([Oo][Rr])|\|")
NOT = re.compile(r"([Nn][Oo][Tt])|~")
LEMMA = re.compile(r"^[А-яЁё][А-яЁё-]*[А-яЁё]$")
IGNORED = re.compile(r" ")

if __name__ == "__main__":
    if (not FILTERED_DIR.is_dir() or not INDEX.is_file() or not TOKENS_FILE.is_file() or not LEMMAS_FILE.is_file() or
            not TOKENS_TF_IDF_FOLDER.is_dir() or not LEMMAS_TF_IDF_FOLDER.is_dir()):
        raise ValueError("Bad constants")

    tf_idf(tokens_tf_idf_folder=TOKENS_TF_IDF_FOLDER, lemmas_tf_idf_folder=LEMMAS_TF_IDF_FOLDER, index_file=INDEX,
           filtered_folder=FILTERED_DIR,
           tokens_selectors=[re.compile(regexp) for regexp in TOKENS_SELECTORS],
           tags_selectors=[re.compile(regexp) for regexp in TAGS_SELECTORS], nlp=LANGUAGE)

    print("\nexit\n")
