import pathlib

from bs4 import BeautifulSoup


def filter_archive(archive_folder: pathlib.Path, index_file: pathlib.Path, filtered_folder: pathlib.Path):
    filenames: [str] = []

    # Получаем список имен файлов из индекса
    with open(index_file, "r") as index:
        # Читаем строку
        for line in index:
            # Делим строку по ": "
            strips = line.split(": ")
            # Добавляем файл
            filenames.append(strips[0])

    # Для каждого файла
    for file_string in filenames:
        with open(archive_folder.joinpath(file_string), "r") as html:
            # Парсим содержимое
            soup = BeautifulSoup(html.read(), "html.parser")

        # Очищаем текст от тегов, заменяем &nbsp на пробелы
        text = soup.get_text('\n', strip=True).replace(" ", " ")

        # Записываем результат
        with open(filtered_folder.joinpath(file_string), "a+") as filtered:
            filtered.write(text)
