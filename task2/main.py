import pathlib
import re
import spacy
import filter
import tokens

TOKENS_FILE: pathlib.Path = pathlib.Path("tokens.txt")
LEMMAS_FILE: pathlib.Path = pathlib.Path("lemmas.txt")
FILTERED_DIR: pathlib.Path = pathlib.Path("../filtered/")
INDEX: pathlib.Path = pathlib.Path("index.txt")
ARCHIVE_DIR: pathlib.Path = pathlib.Path("../archive/")
# NLP пакет для русского языка
LANGUAGE: spacy.Language = spacy.load("ru_core_news_lg")
# Выбираем только слова из русских букв+дефис между частями слов
TOKENS_SELECTORS: [str] = ["^[А-яЁё][А-яЁё-]*[А-яЁё]$"]
# Выбираем только глаголы, существительные, прилагательные
TAGS_SELECTORS: [str] = ["VERB", "PROPN", "NOUN", "ADJ"]

if __name__ == "__main__":
    if not FILTERED_DIR.is_dir() or not ARCHIVE_DIR.is_dir() or not INDEX.is_file():
        raise ValueError
    item = next(FILTERED_DIR.iterdir(), None)
    # Если папка "пуста", то фильтрируем
    if item.name == ".placeholder":
        filter.filter_archive(pathlib.Path(ARCHIVE_DIR), pathlib.Path(INDEX), pathlib.Path(FILTERED_DIR))

    tokens.tokenize(index_file=str(INDEX), filtered_folder=str(FILTERED_DIR),
                    tokens_selectors=[re.compile(regexp) for regexp in TOKENS_SELECTORS],
                    tags_selectors=[re.compile(regexp) for regexp in TAGS_SELECTORS],
                    nlp=LANGUAGE, tokens_file=TOKENS_FILE, lemmas_file=LEMMAS_FILE)
