import pathlib
import re

import spacy


def tokenize(index_file: str, filtered_folder: str, lemmas_file: pathlib.Path, tokens_file: pathlib.Path,
             tokens_selectors: [re.Pattern], tags_selectors: [re.Pattern],
             nlp: spacy.Language):
    # Проходимся по каждому шаблону, если один из них совпадает, возвращаем True
    def is_selected(string: str, regexps: [re.Pattern]):
        for regexp in regexps:
            if re.match(regexp, string):
                return True
        return False

    # "Бассейн" лемм и токенов в виде "лемма":["токен1", "токен2", ...]
    pool = {}
    # Вытаскиваем имена файлов из индекса
    filenames: [str] = []
    with open(index_file, "r") as index:
        # Читаем строку
        for line in index:
            # Делим строку по ": "
            strips = line.split(": ")
            # Добавляем файл
            filenames.append(strips[0])

    # Называется: с генераторами с детства я дружу. Функция добавления в результирующий бассейн
    def add_to_pool(token):
        if token.lemma_ in pool:
            if token.text not in pool[token.lemma_]:
                pool[token.lemma_].append(token.text)
        else:
            pool[token.lemma_] = [token.text]
        return True

    for file_string in filenames:
        # Считываем отфильтрованную выкачку
        with open(pathlib.Path(filtered_folder, file_string), "r") as file:
            doc = file.read()
            # Анализируем отфильтрованное
            tokens = nlp(doc)
            # Добавляем в результат, если токен не из стоп-листа, подходит по одному из шаблонов тэг, подходит по
            # одному из шаблонов токен
            # noinspection PyStatementEffect
            [None for token in tokens if
             not token.is_stop and is_selected(token.pos_, tags_selectors) and is_selected(token.text,
                                                                                           tokens_selectors) and add_to_pool(
                 token)]
            # Для прогресса
            print(f"{file_string} is parsed")

    # Записываем результат
    with open(lemmas_file, "w+") as lem_file, open(tokens_file, "w+") as tok_file:
        for key, value in sorted(pool.items(), key=lambda item: item[0]):
            lem_file.write(''.join([f"{key}:", *[f" {tok}" for tok in value], "\n"]))
            for text in value:
                tok_file.write(f"{text}\n")

def get_lemmas_n_tokens(lemmas_file: pathlib.Path) -> dict[str, list[str]]:
    result: dict[str, list[str]] = {}

    with open(lemmas_file, "r") as lem_file:
        for line in lem_file:
            strips: list[str] = line.split(" ")
            strips[0] = strips[0][:-1]

            result[strips[0]] = strips[1:]

    return result

