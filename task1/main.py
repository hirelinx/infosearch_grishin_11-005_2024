import math
from pathlib import Path
import re

import requests
from bs4 import BeautifulSoup


def crawl(main_url: str, banned_urls: [re.Pattern], index_file: Path, paths: [Path]):
    # Функция проверки: не забанен ли нами этот url
    def banned(_url: str) -> bool:
        for ban in banned_urls:
            if re.match(ban, _url):
                return True
        return False

    # Очередь сайтов для посещения
    queue = [main_url]
    # Посещенные сайты
    visited = []

    for i in range(len(paths)):
        # Проверяем urlы, пока не найдем валидный
        while True:
            while True:
                # Получаем очередной url
                current_url = queue.pop()
                # Пооверяем, не посещен ли url
                if current_url not in visited:
                    break
            # Делаем запрос
            response = requests.get(current_url)

            # Проверяем, что страница не css, javascript и т.д.
            if response.headers['Content-Type'][0:10] != "text/html;":
                # Если да, то больше ее не посещаем
                visited.append(current_url)
                continue
            break

        # Парсим html
        soup = BeautifulSoup(response.content, "html.parser")

        # Записываем html в файл-выкачку
        with open(paths[i], "w+") as file:
            file.write(str(soup))

        # Добавляем строку в индекс, можно было наверно не тыщу раз файл открывать, но мне лень переделывать
        with open(index_file, "a+") as index:
            zeros = "".join(["0"] * (len(str(stop - start + 1)) - len(str(i + 1))))
            index.write(f"{zeros}{i + 1}.txt: {current_url}\n")

        # Выбираем элементы-ссылки
        link_elements = soup.select("a[href]")

        # Ищем ссылки на внутренние ресурсы сайта
        for link_element in link_elements:
            # Выбираем ссылку
            url = link_element["href"]
            # Почему-то попадались пустые ссылки, ну ладно
            if len(url) == 0:
                continue
            # Ссылка указывает внутрь сайта?
            if url[0] == '/':
                # Полный url сайта
                full = main_url + url[1:]
                # Проверка на бан url
                if banned(full):
                    continue
                # Добавляем в очередь
                queue.append(full)
                continue
            # Ссылка указывает внутрь сайта?
            if main_url in url:
                # Проверка на бан url
                if banned(url):
                    continue
                # Добавляем в очередь
                queue.append(url)
        # Добавляем ткущий url в посещенные
        visited.append(current_url)
        # Счетчик, просто для дебага
        print("{}".format(i + 1))


def load_index(index_file: Path):
    result: dict[str, str] = {}

    with open(index_file, "r") as index:
        for line in index:
            strips = line[:-1].split(" ")
            result[strips[0][:-1]] = strips[1]

    return result


# Номера файлов-выкачек
start = 1
stop = 1001

if __name__ == '__main__':
    # Кроулим https://башорг.рф; баним комиксы, предложку, поиск; указываем путь к индексу; указываем путь к файлам-выкачке
    crawl("https://xn--80abh7bk0c.xn--p1ai/", [re.compile(string) for string in
                                               ["https://xn--80abh7bk0c.xn--p1ai/strip*",
                                                "https://xn--80abh7bk0c.xn--p1ai/add*",
                                                "https://xn--80abh7bk0c.xn--p1ai/search*"
                                                "https://xn--80abh7bk0c.xn--p1ai/random*",
                                                "^https://xn--80abh7bk0c.xn--p1ai/abyss$",
                                                "^https://xn--80abh7bk0c.xn--p1ai/best$",
                                                "https://xn--80abh7bk0c.xn--p1ai/abysstop"
                                                ]],
          Path("index.txt"),
          [Path("../archive/" + stringI + ".txt") for stringI in
           ["".join(["0"] * (math.floor(math.log10(stop - start + 1)) - math.floor(math.log10(i)))) + str(i) for i in
            range(start, stop)]])
